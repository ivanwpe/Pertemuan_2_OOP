/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivan.latihan_oop;

/**
 *
 * @author ivan
 */
public class Enemy extends Hero{
    private int HP;
    private int damage;

    public Enemy(int HP, int damage) {
        this.HP = HP;
        this.damage = damage;
    }

    public int getHP() {
        return HP;
    }

    public void setHP(int HP) {
        this.HP = HP;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }
}
