/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivan.latihan_oop;
import java.util.*;
/**
 *
 * @author ivan
 */
public class Specific_Job extends Hero{
    private int HP;
    private int MP;
    private String Weapon;
    private int damage;
    private int heal;

    public Specific_Job(int HP, int MP, String Weapon, int damage, int heal) {
        this.HP = HP;
        this.MP = MP;
        this.Weapon = Weapon;
        this.damage = damage;
        this.heal = heal;
    }

    public int getHP() {
        return HP;
    }

    public void setHP(int HP) {
        this.HP = HP;
    }

    public int getMP() {
        return MP;
    }

    public void setMP(int MP) {
        this.MP = MP;
    }

    public String getWeapon() {
        return Weapon;
    }

    public void setWeapon(String Weapon) {
        this.Weapon = Weapon;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }
    
    public int getHeal() {
        return heal;
    }

    public void setHeal(int heal) {
        this.heal = heal;
    }
}
