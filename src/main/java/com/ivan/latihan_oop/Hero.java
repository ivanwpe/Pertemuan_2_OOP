/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivan.latihan_oop;
import java.util.*;
/**
 *
 * @author ivan
 */
public class Hero {
    
    public static ArrayList<Object> jobArr = new ArrayList<>();
    public static Scanner scan = new Scanner(System.in); 
    
    public static void main(String[] args){
        int menu = 0;
        String name;
        
        Specific_Job warrior = new Specific_Job(250, 10, "Sword", 30, 5);
        Specific_Job ranger = new Specific_Job(150, 20, "Bow", 20, 10);
        Specific_Job mage = new Specific_Job(100, 30, "Harry Potter's Stick", 15, 15);
        Specific_Job custom = new Specific_Job(50, 30, "pencil", 40, 15);
        
        Enemy enemy = new Enemy(200, 25);

        String job[] = {"Warrior", "Ranger", "Mage"};
        
        do{
            System.out.println("MENU");
            System.out.println("1. Create Hero");
            System.out.println("2. View Spec");
            System.out.println("3. Fight");
            System.out.println("4. Exit");
            System.out.print("-> Choose : ");
            menu = scan.nextInt();

            switch(menu){
                case 1:
                    System.out.println("-> 1. " + job[0]);
                    System.out.println("   2. " + job[1]);
                    System.out.println("   3. " + job[2]);
                    System.out.println("   4. Etc.");
                    System.out.print("-> Input : ");
                    int input = scan.nextInt();
                    
                    switch (input){
                        case 1:
                            System.out.println("");
                            System.out.print("Input Name : ");
                            name = scan.next();
                            System.out.println("Hi " + name + " your job is a " + job[0]);
                            System.out.println("HP     : " + warrior.getHP());
                            System.out.println("MP     : " + warrior.getMP());
                            System.out.println("Weapon : " + warrior.getWeapon());
                            System.out.println("-----------------------------------");
                            System.out.println("");
                            System.out.println(""); 
                            jobArr.add(job);
                            break;
                        case 2:
                            System.out.println("");
                            System.out.print("Input Name : ");
                            name = scan.next();
                            System.out.println("Hi " + name + " your job is a " + job[1]);
                            System.out.println("HP     : " + ranger.getHP());
                            System.out.println("MP     : " + ranger.getMP());
                            System.out.println("Weapon : " + ranger.getWeapon());
                            System.out.println("-----------------------------------");
                            System.out.println("");
                            System.out.println("");
                            jobArr.add(job);
                            break;
                        case 3:
                            System.out.println("");
                            System.out.print("Input Name : ");
                            name = scan.next();
                            System.out.println("Hi " + name + " your job is a " + job[2]);
                            System.out.println("HP     : " + mage.getHP());
                            System.out.println("MP     : " + mage.getMP());
                            System.out.println("Weapon : " + mage.getWeapon());
                            System.out.println("-----------------------------------");
                            System.out.println("");
                            System.out.println("");
                            jobArr.add(job);
                            break;
                        case 4:
                            String annJob;
                            System.out.println("");
                            System.out.print("Input Name : ");
                            name = scan.next();
                            System.out.print("Input your job : ");
                            annJob = scan.next();
                            System.out.println("Hi " + name + " your job is a " + annJob);
                            System.out.println("HP     : " + custom.getHP());
                            System.out.println("MP     : " + custom.getMP());
                            System.out.println("Weapon : " + custom.getWeapon());
                            System.out.println("-----------------------------------");
                            System.out.println("");
                            System.out.println("");
                            jobArr.add(job);
                            break;
                            
                    }
                    break;
                    
                case 2:
                    System.out.println("");
                    System.out.println("1. Warrior");
                    System.out.println("2. Ranger");
                    System.out.println("3. Mage");
                    System.out.println("4. Custom");
                    System.out.print("Choose : ");
                    int choose = scan.nextInt();
                    
                    switch(choose){
                        case 1:
                            System.out.println("");
                            System.out.println("Warrior ");
                            System.out.println("=============");
                            System.out.println("HP     : " + warrior.getHP());
                            System.out.println("MP     : " + warrior.getMP());
                            System.out.println("Weapon : " + warrior.getWeapon());
                            System.out.println("Damage : " + warrior.getDamage());
                            System.out.println("");
                            System.out.println(""); 
                            break;
                        case 2:
                            System.out.println("");
                            System.out.println("Ranger ");
                            System.out.println("=============");
                            System.out.println("HP     : " + ranger.getHP());
                            System.out.println("MP     : " + ranger.getMP());
                            System.out.println("Weapon : " + ranger.getWeapon());
                            System.out.println("Damage : " + ranger.getDamage());
                            System.out.println("");
                            System.out.println(""); 
                            break;
                        case 3:
                            System.out.println("");
                            System.out.println("Mage ");
                            System.out.println("=============");
                            System.out.println("HP     : " + mage.getHP());
                            System.out.println("MP     : " + mage.getMP());
                            System.out.println("Weapon : " + mage.getWeapon());
                            System.out.println("Damage : " + mage.getDamage());
                            System.out.println("");
                            System.out.println(""); 
                            break;
                        case 4:
                            System.out.println("");
                            System.out.println("Custom ");
                            System.out.println("=============");
                            System.out.println("HP     : " + custom.getHP());
                            System.out.println("MP     : " + custom.getMP());
                            System.out.println("Weapon : " + custom.getWeapon());
                            System.out.println("Damage : " + custom.getDamage());
                            System.out.println("");
                            System.out.println(""); 
                            break;
                    }
                    break;
                    
                case 3:
                    String hero = "";
                    
                    System.out.println("");
                    System.out.println("We're sorry, we lost your data.");
                        
                    do{
                        System.out.print("What is your Hero type? [Warrior/Ranger/Mage/Custom]");
                        hero = scan.next();
                        System.out.println("");
                    }while(hero.contentEquals("Warrior") == false && 
                            hero.contentEquals("Ranger") == false &&
                            hero.contentEquals("Mage") == false &&
                            hero.contentEquals("Custom") == false);
                    
                    do{                       
                        if(hero.contentEquals("Warrior") == true){
                            System.out.println("Your HP  : " + warrior.getHP());
                            System.out.println("Enemy HP : " + enemy.getHP());
                            System.out.println("");
                            System.out.println("1. Attack");
                            System.out.println("2. Heal");
                            System.out.print("-> ");
                            int input1 = scan.nextInt();
                            
                            if(input1 == 1){
                                int enemyHp1 = enemy.getHP();
                                enemyHp1 = enemyHp1 - warrior.getDamage();
                                enemy.setHP(enemyHp1);
                                
                                int yourHp1 = warrior.getHP();
                                yourHp1 = yourHp1 - enemy.getDamage();
                                warrior.setHP(yourHp1);
                                
                                if(yourHp1 < 0){
                                    yourHp1 = 0;
                                } 
                            }
                            else if(input1 == 2){
                                int yourHp2 = warrior.getHP();
                                yourHp2 = warrior.getHP() + warrior.getHeal();
                                warrior.setHP(yourHp2);                            
                            }
                        }
                        
                        if(hero.contentEquals("Ranger") == true){
                            System.out.println("Your HP  : " + ranger.getHP());
                            System.out.println("Enemy HP : " + enemy.getHP());
                            System.out.println("");
                            System.out.println("1. Attack");
                            System.out.println("2. Heal");
                            System.out.print("-> ");
                            int input1 = scan.nextInt();
                            
                            if(input1 == 1){
                                int enemyHp1 = enemy.getHP();
                                enemyHp1 = enemyHp1 - ranger.getDamage();
                                enemy.setHP(enemyHp1);
                                
                                int yourHp1 = ranger.getHP();
                                yourHp1 = yourHp1 - enemy.getDamage();
                                ranger.setHP(yourHp1);
                                
                                if(yourHp1 < 0){
                                    yourHp1 = 0;
                                }
                            }
                            else if(input1 == 2){
                                int yourHp2 = ranger.getHP();
                                yourHp2 = ranger.getHP() + ranger.getHeal();
                                ranger.setHP(yourHp2);                            
                            }
                        }
                        
                        if(hero.contentEquals("Mage") == true){
                            System.out.println("Your HP  : " + mage.getHP());
                            System.out.println("Enemy HP : " + enemy.getHP());
                            System.out.println("");
                            System.out.println("1. Attack");
                            System.out.println("2. Heal");
                            System.out.print("-> ");
                            int input1 = scan.nextInt();
                            
                            if(input1 == 1){
                                int enemyHp1 = enemy.getHP();
                                enemyHp1 = enemyHp1 - mage.getDamage();
                                enemy.setHP(enemyHp1);
                                
                                int yourHp1 = mage.getHP();
                                yourHp1 = yourHp1 - enemy.getDamage();
                                mage.setHP(yourHp1);
                                
                                if(yourHp1 < 0){
                                    yourHp1 = 0;
                                }
                            }
                            else if(input1 == 2){
                                int yourHp2 = mage.getHP();
                                yourHp2 = mage.getHP() + mage.getHeal();
                                mage.setHP(yourHp2);                            
                            }
                        }
                        
                        if(hero.contentEquals("Custom") == true){
                            System.out.println("Your HP  : " + warrior.getHP());
                            System.out.println("Enemy HP : " + enemy.getHP());
                            System.out.println("");
                            System.out.println("1. Attack");
                            System.out.println("2. Heal");
                            System.out.print("-> ");
                            int input1 = scan.nextInt();
                            
                            if(input1 == 1){
                                int enemyHp1 = enemy.getHP();
                                enemyHp1 = enemyHp1 - custom.getDamage();
                                enemy.setHP(enemyHp1);
                                
                                int yourHp1 = custom.getHP();
                                yourHp1 = yourHp1 - enemy.getDamage();
                                custom.setHP(yourHp1);
                                
                                if(yourHp1 < 0){
                                    yourHp1 = 0;
                                }
                            }
                            else if(input1 == 2){
                                int yourHp2 = custom.getHP();
                                yourHp2 = custom.getHP() + custom.getHeal();
                                custom.setHP(yourHp2);                            
                            }
                        }                                           
                        
                    }while(warrior.getHP() > 0 && ranger.getHP() > 0 && mage.getHP() > 0 &&
                            custom.getHP() > 0 && enemy.getHP() > 0);                                          
                    
                    if(enemy.getHP() < 0){
                            System.out.println("YOU WIN");
                            System.out.println("");
                            System.out.println("");
                    }else{
                            System.out.println("YOU LOSE");
                            System.out.println("");
                            System.out.println("");
                        }
                    
                    break;

                    
                case 4:
                    break;
            }
        
        }while(menu != 4);
    }
}
